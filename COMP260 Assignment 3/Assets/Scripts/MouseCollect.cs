﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCollect : MonoBehaviour {

	public WindowLight window;
	private BossMouse boss;

	// Use this for initialization
	void Start () {
		boss = FindObjectOfType<BossMouse> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D(Collider2D collider) {
		window.lightOn = false;
		boss.numMice--;
		Destroy(gameObject);
	}
}
