﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (SpriteRenderer))]
public class HealthBarTracker : MonoBehaviour {

	public GameObject player;
	private Image img;
	public Sprite[] healthBar;
	private int currHealth;

	// Use this for initialization
	void Start () {
		img = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {                        //code idea from https://answers.unity.com/questions/968526/changing-sprites-depending-on-int-value.html
		currHealth = player.GetComponent<PlayerMove> ().currentHealth;

		switch (currHealth) {
		case 0: 
			img.sprite = healthBar [0];
			break;
		case 1:
			img.sprite = healthBar [1];
			break;
		case 2:
			img.sprite = healthBar [2];
			break;
		case 3:
			img.sprite = healthBar [3];
			break;
		case 4:
			img.sprite = healthBar [4];
			break;
		case 5:
			img.sprite = healthBar [5];
			break;
		case 6:
			img.sprite = healthBar [6];
			break;
		case 7:
			img.sprite = healthBar [7];
			break;
		case 8:
			img.sprite = healthBar [8];
			break;
		case 9:
			img.sprite = healthBar [9];
			break;
		default:
			img.sprite = healthBar [9];
			break;
		}
	}
}
