﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class KillOnHit : MonoBehaviour {

	private PlayerMove player;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerMove> ();
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider) {
		audio.Play();
		player.GetComponent<PlayerMove> ().currentHealth = 0;
		player.GetComponent<PlayerMove> ().GameOver ();
	}
}
