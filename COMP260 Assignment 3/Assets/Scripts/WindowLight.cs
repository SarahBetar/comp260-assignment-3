﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (SpriteRenderer))]
public class WindowLight : MonoBehaviour {
	
	public bool lightOn = false;
	SpriteRenderer rend;
	public Sprite[] windowLight;

	// Use this for initialization
	void Start () {
		rend = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (lightOn) {
			rend.sprite = windowLight [0];
		} else {
			rend.sprite = windowLight [1];
		}			
	}

}
