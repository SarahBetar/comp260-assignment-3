﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisSpawner : MonoBehaviour {

	public DebrisMove debrisPrefab;
	public int maxDebris = 10;
	public int currDebris = 0;
	public Rect spawnRect;
	public GameObject player;

	private float timer = 0;
	public float debrisSpawnDelay = 1;

	private int numToSpawn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeScale == 0 || !player.GetComponent<PlayerMove> ().startTimer) {
			return;					
		}


		timer += Time.deltaTime;
		if (timer > debrisSpawnDelay && currDebris < maxDebris) {
			numToSpawn = Random.Range (1, 3);
			for (int i = 0; i < numToSpawn; i++) {
				SpawnDebris (currDebris + 1);
			}
			timer = 0;
		}
	}

	void OnDrawGizmos() {
		// draw the spawning rectangle
		Gizmos.color = Color.green;
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMin));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMin));
	}

	void SpawnDebris(int i) {
		float x = spawnRect.xMin + 
			Random.value * spawnRect.width;
		float y = spawnRect.yMin;
		DebrisMove debris = Instantiate (debrisPrefab, new Vector2(x, y), Quaternion.identity);
		debris.transform.parent = transform;
		debris.gameObject.name = "Debris " + i;
		currDebris++;
	}

}
