﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
public class DebrisMove : MonoBehaviour {

	private float speed;
	private Vector2 move;
	private Rigidbody2D rigidbody;


	public float speedMin = 100;
	public float speedMax = 200;

	DebrisSpawner spawner;

	private float timer = 0;
	public float expiryTime = 7;

	// Use this for initialization
	void Start () {
		speed = Random.Range (speedMin, speedMax);
		rigidbody = GetComponent<Rigidbody2D>();
		gameObject.layer = 12;
		spawner = FindObjectOfType<DebrisSpawner> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 move = speed * Time.deltaTime * Vector2.down;
		rigidbody.velocity = move;
		timer += Time.deltaTime;
		if (timer > expiryTime) {
			KillObject();
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		KillObject();
	}

	void KillObject() {
		Destroy (gameObject);
		spawner.GetComponent<DebrisSpawner> ().currDebris--;
	}
}
