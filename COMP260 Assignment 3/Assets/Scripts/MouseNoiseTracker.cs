﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MouseNoiseTracker : MonoBehaviour {

	static private MouseNoiseTracker instance;
	private int numMiceMissed = 0;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		if (instance == null) {
			instance = this;
		} else {
			Destroy(gameObject);
		}
		audio.volume = numMiceMissed / 10.0f;
	}

	static public MouseNoiseTracker Instance {
		get { return instance; }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setMiceMissed(int i) {
		numMiceMissed += i;
		audio.volume = numMiceMissed / 10.0f;
		audio.Play();
	}

	public int getMiceMissed() {
		return numMiceMissed;
	}

	void Awake() {
		DontDestroyOnLoad (this.gameObject);
	}
}
