﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour {

	public Text missed;

	// Use this for initialization
	void Start () {
		missed.enabled = false;
		if (MouseNoiseTracker.Instance.getMiceMissed () > 0) {
			missed.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
