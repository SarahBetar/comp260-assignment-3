﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class BossMouse : MonoBehaviour {

	private MouseCollect[] mice;
	public int numMice = 0;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		mice = FindObjectsOfType<MouseCollect> ();
		audio = GetComponent<AudioSource> ();
		numMice = mice.Length;
	}
	
	// Update is called once per frame
	void Update () {
		if (numMice <= 0) {
			audio.Stop ();
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		MouseNoiseTracker.Instance.setMiceMissed (numMice);
		if (SceneManager.GetActiveScene ().name == "Lvl1") {
			SceneManager.LoadScene ("Lvl2", LoadSceneMode.Single);
		} else if (SceneManager.GetActiveScene ().name == "Lvl2") {
			SceneManager.LoadScene ("Lvl3", LoadSceneMode.Single);
		} else if (SceneManager.GetActiveScene ().name == "Lvl3") {
			SceneManager.LoadScene ("Lvl4", LoadSceneMode.Single);
		} else if (SceneManager.GetActiveScene ().name == "Lvl4") {
			SceneManager.LoadScene ("Lvl5", LoadSceneMode.Single);
		}
	}
}
