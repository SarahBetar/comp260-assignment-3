﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (CircleCollider2D))]
[RequireComponent(typeof(AudioSource))]
public class PlayerMove : MonoBehaviour {

	public float moveSpeed = 7.0f;
	public float jumpHeight = 9.0f;
	private bool jumping = false;
	private Rigidbody2D rigidbody;
	private SpriteRenderer rend;
	private CircleCollider2D col;
	private AudioSource audio;
	public AudioClip pickupClip;

	//Used for a rectangle at the characters feet which is used to check if player is currently jumping
	public LayerMask windowLayer;
	public Transform topLeft;
	public Transform bottomRight;

	//Health System
	public int playerStartHealth = 9;
	public int currentHealth;
	public LayerMask debrisLayer;
	public LayerMask spikesLayer;
	public LayerMask miceLayer;

	//Timer System
	public float timer = 120;
	public bool startTimer = false;
	public Text timerText;

	//Camera chage
	public Camera entireLvl;
	public Camera playerCam;

	//Game Over System
	public GameObject shellPanel;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
		rend = GetComponent<SpriteRenderer> ();
		col = GetComponent<CircleCollider2D> ();
		currentHealth = playerStartHealth;
		Updatetimer ();
		shellPanel.SetActive (false);
		Time.timeScale = 1;
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!startTimer && Input.GetAxis ("Start") != 0) { //if timer hasn't started and player moves/jumps
			startTimer = true;
			entireLvl.enabled = false;
			playerCam.enabled = true;
		}

		if (startTimer && timer > 0) { //if timer has started and is not at zero
			timer -= Time.deltaTime;
		}

		if (startTimer) {
			if (timer <= 0) {
				GameOver ();
			}

			if (timer > 0) {
				Updatetimer ();
			}

			if (Input.GetAxis ("Horizontal") < 0) {
				rend.flipX = false;
			} else if (Input.GetAxis ("Horizontal") > 0) {
				rend.flipX = true;
			}

			if (Input.GetAxis ("Vertical") < 0) {
				col.isTrigger = true;
			}
		}

		if(currentHealth <= 0) {
			GameOver ();
		}
	}

	void FixedUpdate() {
		if (startTimer) {
			jumping = !(Physics2D.OverlapArea (topLeft.position, bottomRight.position, windowLayer)); //code taken from https://answers.unity.com/questions/707526/check-if-2d-character-is-grounded.html
			Vector2 velocity = rigidbody.velocity;

			velocity.x = Input.GetAxis ("Horizontal") * moveSpeed;	//Move player in direction of horizontal input (left or right) at moveSpeed

			if (!jumping && Input.GetButtonDown ("Jump")) {	 //if the player is not already jumping and presses jump					
				velocity.y = jumpHeight;							
			}

			rigidbody.velocity = velocity;
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if ((debrisLayer.Contains (collision.gameObject) || spikesLayer.Contains (collision.gameObject))&& currentHealth > 0) {
			currentHealth--;
			audio.Play();
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (miceLayer.Contains (collider.gameObject)) {
			audio.PlayOneShot (pickupClip);
		}
	}

	public void GameOver() {
		Time.timeScale = 0;
		shellPanel.SetActive (true);
	}

	public void OnRestart() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		shellPanel.SetActive (false);
		Time.timeScale = 1;
	}

	void Updatetimer() {
		int m = Mathf.FloorToInt(timer / 60);
		int s = Mathf.FloorToInt(timer - m * 60);
		string minutes = m.ToString ("00");
		string seconds = s.ToString ("00");
		timerText.text = minutes + ":" + seconds;
	}

	void OnTriggerExit2D(Collider2D collider) {
		col.isTrigger = false;
	}	
}
